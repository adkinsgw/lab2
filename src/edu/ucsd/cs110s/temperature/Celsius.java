package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature
{

	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TO DO
		return "" + getValue();
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	
	@Override
	public Temperature toFahrenheit() {
		float temp = getValue();
		temp = temp*9/5+32;
		return new Celsius(temp);
	}
}
