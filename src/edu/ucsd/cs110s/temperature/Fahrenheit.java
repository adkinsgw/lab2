package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature 
{
	public Fahrenheit(float t) 
	{
		super(t);
	}
	public String toString()
	{
		// TO DO
		return "" + getValue();
	}
	@Override
	public Temperature toCelsius() {
		float temp = getValue();
		temp = (temp-32)*5/9;
		return new Fahrenheit(temp);
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
}
	
